//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#import "___FILEBASENAME___.h"

#define debug 0
@interface ___FILEBASENAMEASIDENTIFIER___ ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation ___FILEBASENAMEASIDENTIFIER___

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self config];
}

#pragma mark - Config

-(void) config
{
    [self registerCell];
}

-(void) registerCell
{

}

#pragma mark - LoadData

#pragma mark - IBAction

#pragma mark - Delegate


//#pragma mark - TableView

//-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    return <#Number row#>
//}
//
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    <#CustomCell#> *cell = [tableView dequeueReusableCellWithIdentifier:<#Identifier#>];
//
//    return cell;
//}
#pragma mark - Gesture

#pragma mark - Utils


@end
