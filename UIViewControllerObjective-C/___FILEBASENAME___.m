//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//	___COPYRIGHT___
//

#import "___FILEBASENAME___.h"

#define debug 0
@interface ___FILEBASENAMEASIDENTIFIER___ ()

@end

@implementation ___FILEBASENAMEASIDENTIFIER___

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self config];
}

// MARK: - Config

-(void) config
{
    
}

// MARK: - LoadData

// MARK: - IBAction

// MARK: - Delegate

// MARK: - Gesture

// MARK: - Utils



@end
