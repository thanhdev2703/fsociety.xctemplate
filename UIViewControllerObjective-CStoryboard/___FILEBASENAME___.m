//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#import "___FILEBASENAME___.h"

#define debug 0
@interface ___FILEBASENAMEASIDENTIFIER___ ()

@end

@implementation ___FILEBASENAMEASIDENTIFIER___

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self config];
}

#pragma mark - Config

-(void) config
{
    
}

#pragma mark - LoadData

#pragma mark - IBAction

#pragma mark - Delegate

#pragma mark - Gesture

#pragma mark - Utils

#pragma mark - Create
+(___FILEBASENAME___ *) newVC
{
    UIStoryboard *st = [UIStoryboard storyboardWithName:@"___VARIABLE_storyboardName___" bundle:nil];
    
    return [st instantiateViewControllerWithIdentifier:@"___FILEBASENAME___"];
}
@end
